import { Component } from '@angular/core';

@Component({
  selector: 'core-component',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core.component';
import { BoxMetaModule } from '../features/box-meta/box-meta.module'
import { BoxOpeningsModule } from '../features/box-openings/box-openings.module';
import { BoxItemGridModule } from '../features/box-item-grid/box-item-grid.module';

@NgModule({
  imports: [
    CommonModule,
    BoxOpeningsModule,
    BoxItemGridModule,
    BoxMetaModule,
    // BoxItemModule
  ],
  declarations: [CoreComponent],
  exports: [CoreComponent]
})
export class CoreModule {

  constructor() {
    console.log('u')

  }


}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxItemGridComponent } from './box-item-grid.component';

describe('BoxItemGridComponent', () => {
  let component: BoxItemGridComponent;
  let fixture: ComponentFixture<BoxItemGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxItemGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxItemGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

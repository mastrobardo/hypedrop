import { Component, OnInit } from '@angular/core';
import { ListCardItemComponent } from '../common-components/list-card-item/list-card-item.component';
import { DataLoaderService } from '../../services/data-loader.service';
import { Edges } from '../common-components/common-interfaces';

@Component({
  selector: 'box-item-grid',
  templateUrl: './box-item-grid.component.html',
  styleUrls: ['./box-item-grid.component.scss']
})
export class BoxItemGridComponent implements OnInit {

  private dataService: DataLoaderService;
  private boxes: Edges;

  constructor(data: DataLoaderService) {
    this.dataService = data;
   }

  ngOnInit() {
    this.dataService.loadBoxViewItems().subscribe(val => {
      return this.boxes = (val as any).data.boxItems.edges;
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxItemGridComponent } from './box-item-grid.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { ListCardItemModule } from '../common-components/list-card-item/list-card-item.module';



@NgModule({
  declarations: [BoxItemGridComponent],
  exports: [BoxItemGridComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    ListCardItemModule
  ]
})
export class BoxItemGridModule { }

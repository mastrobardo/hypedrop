import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxMetaComponent } from './box-meta.component';

describe('BoxMetaComponent', () => {
  let component: BoxMetaComponent;
  let fixture: ComponentFixture<BoxMetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxMetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

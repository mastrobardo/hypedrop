import { Component, OnInit } from '@angular/core';
import { DataLoaderService } from '../../services/data-loader.service';

@Component({
  selector: 'box-meta',
  templateUrl: './box-meta.component.html',
  styleUrls: ['./box-meta.component.scss']
})
export class BoxMetaComponent implements OnInit {

  private dataService: DataLoaderService;
  private boxViewMeta: object;

  constructor(data: DataLoaderService) {
    this.dataService = data;
  }

  ngOnInit() {
    this.dataService.loadBoxViewMeta().subscribe(val => {
      return this.boxViewMeta = val;
    });
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxMetaComponent } from './box-meta.component'
import { MatCardModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    MatCardModule
  ],
  declarations: [BoxMetaComponent],
  exports: [BoxMetaComponent]
})
export class BoxMetaModule {
  constructor() {
    console.log('box meta module')
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxOpeningsComponent } from './box-openings.component';

describe('BoxOpeningsComponent', () => {
  let component: BoxOpeningsComponent;
  let fixture: ComponentFixture<BoxOpeningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxOpeningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxOpeningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

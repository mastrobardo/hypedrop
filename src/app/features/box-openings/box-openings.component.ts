import { Component, OnInit } from '@angular/core';
import { DataLoaderService } from '../../services/data-loader.service';
import { Edges } from '../common-components/common-interfaces';
import { ListCardItemComponent } from '../common-components/list-card-item/list-card-item.component';

@Component({
  selector: 'box-openings',
  templateUrl: './box-openings.component.html',
  styleUrls: ['./box-openings.component.scss']
})
export class BoxOpeningsComponent implements OnInit {

  private dataService: DataLoaderService;
  private boxOpenings: Edges;
  private selected: ListCardItemComponent;

  constructor(data: DataLoaderService) {
    this.dataService = data;
  }

  ngOnInit() {
    this.dataService.loadBoxStream().subscribe(val => {
      return this.boxOpenings = val.data.boxOpenings.edges;
    });
  }

  selectEventHander(target: ListCardItemComponent) {
    this.selected && this.selected.sel(false);
    this.selected = target;
    this.selected.sel(true);
  }


}

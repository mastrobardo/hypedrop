import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxOpeningsComponent } from './box-openings.component'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { ListCardItemModule } from '../common-components/list-card-item/list-card-item.module';
import { MatCardModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    ListCardItemModule
  ],
  declarations: [BoxOpeningsComponent],
  exports: [BoxOpeningsComponent]
})
export class BoxOpeningsModule { }

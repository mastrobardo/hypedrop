
export interface Box {
    id: string;
    iconUrl: string;
    slug: string;
}

export interface Edges {
    [index: number]:  Box
}

export interface CommonData {
    boxOpenings: {
        edges: Edges
    }
} 
export interface BoxOpenings {
    data: CommonData
}


import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Box } from '../common-interfaces';

@Component({
  selector: 'list-card-item',
  templateUrl: './list-card-item.component.html',
  styleUrls: ['./list-card-item.component.scss']
})
export class ListCardItemComponent implements OnInit {
  @Input() box: any;
  @Input() hasBack: boolean;
  @Input() staySelected: boolean;
  @Output() selectEvent = new EventEmitter<ListCardItemComponent>();
  constructor() { }

  private alternateView: any;
  public id: string;
  public isSelected: boolean = false;

  ngOnInit() {
    this.setUpConfig(Object.assign({}, this.box));
  }

  setUpConfig(config: any) {
    if(this.hasBack ) {
      this.box = config.node.itemVariant;
      this.alternateView = config.node.box;
      this.alternateView.user = config.node.user.displayName;
    }else{
      this.box = this.alternateView = config.node.item
    }
    if(this.staySelected) {
      this.isSelected = true;
    }
  }

  onClick(evt: Event) {
    this.selectEvent.emit(this);
  }

  public sel(bool: boolean) {
    if(this.staySelected) {
      this.isSelected = true;
    }
    this.isSelected = bool;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListCardItemComponent} from './list-card-item.component'
import { MatCardModule } from '@angular/material';



@NgModule({
  declarations: [ListCardItemComponent],
  exports:[ListCardItemComponent],
  imports: [
    CommonModule,
    MatCardModule
  ]
})
export class ListCardItemModule { }

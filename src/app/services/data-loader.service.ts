import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BOXLIST_STREAM_URL, BOXLIST_URL, BOXLIST_VIEW_ITEMS_URL, BOXLIST_VIEW_META_URL } from './constants'
import { BoxOpenings } from '../features/common-components/common-interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLoaderService {

  private boxList: object;


  constructor(private httpClient: HttpClient) {}

  private loadData(url: string) {
    console.log('loading from', url);
    return this.httpClient.get(url);
  }

  public loadBoxList() {
    return this.loadData(BOXLIST_URL)
  }

  public loadBoxStream() {
    return this.httpClient.get<BoxOpenings>(BOXLIST_STREAM_URL);
  }

  public loadBoxViewItems(){
    return this.loadData(BOXLIST_VIEW_ITEMS_URL)
  }

  public loadBoxViewMeta(){
    return this.loadData(BOXLIST_VIEW_META_URL)
  }
}
